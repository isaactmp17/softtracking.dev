<?php

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $item=new User();
        $item->name='Isaac Petruccelli';
        $item->email='isaactmp@gmail.com';
        $item->password=Hash::make('22yYe7Kg');
        $item->role='admin';
        $item->save();

        $item=new User();
        $item->name='Mariano Pineda';
        $item->email='web@ofimax.org';
        $item->password=Hash::make('yPTtyrZv');
        $item->role='admin';
        $item->save();

        $item=new User();
        $item->name='Administrador';
        $item->email='admin@alliancetradingbureau.com';
        $item->password=Hash::make('aeWraYx3');
        $item->role='admin';
        $item->save();
    }
}
