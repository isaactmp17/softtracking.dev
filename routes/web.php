<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/**
Route::get('/', function () {
    return view('welcome');
});**/

//Auth::routes();

Route::group(['middleware' => ['auth']], function () {
    Route::group(['prefix' => 'admin', 'middleware' => ['admin']], function () {
        Route::get('/', 'Admin\DashboardController@index');
        Route::resource('/shipping', 'Admin\ShippingController')->except([
            'destroy'
        ]);
        Route::post('/shipping/update_status', 'Admin\ShippingController@updateStatus');
    });
    Route::group(['prefix' => 'client', 'middleware' => ['client']], function () {
        Route::get('/', 'Client\DashboardController@index');
        Route::post('/profile/update2', 'Client\DashboardController@update2');
    });
});



// Socialite
Route::get('/login/google', 'Auth\LoginController@google');
Route::get('/login/google/redirect', 'Auth\LoginController@googleRedirect');
Route::get('/login/facebook', 'Auth\LoginController@facebook');
Route::get('/login/facebook/redirect', 'Auth\LoginController@facebookRedirect');

// Authentication Routes...
Route::get('/', 'Auth\LoginController@showLoginForm')->name('login-page');
Route::post('/login', 'Auth\LoginController@login')->name('login');
Route::post('/logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes...
Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('/register', 'Auth\RegisterController@register');

// Password Reset Routes...
//Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
//Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
//Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
//Route::post('password/reset', 'Auth\ResetPasswordController@reset');
