<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('page_title', env('APP_NAME'))</title>
    @yield('seo')
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}" type="image/x-icon">
    <link rel="icon" href="{{ asset('favicon.ico') }}" type="image/x-icon">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
    <link rel="stylesheet" href="/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
    <link rel="stylesheet" href="/plugins/jqvmap/jqvmap.min.css">
    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/mdb.min.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/adminlte.min.css')}}">
    <link rel="stylesheet" href="/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
    <link rel="stylesheet" href="/plugins/daterangepicker/daterangepicker.css">
    <link rel="stylesheet" href="/plugins/summernote/summernote-bs4.css">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="//cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css" rel="stylesheet">
    @yield('head_styles')
</head>
<body class="hold-transition sidebar-mini sidebar-collapse">
<div class="wrapper">
    @include('layouts.menu_dashboard')
    @yield('content')
    <footer class="main-footer">
        <strong>Copyright &copy; 2020 ATB Panel.</strong>
        All rights reserved.
    </footer>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Control sidebar content goes here -->
    </aside>
</div>
@if(Auth::user()->role!='admin')
    @if(is_null(Auth::user()->address)&&is_null(Auth::user()->phone))
        <div class="modal fade" id="modalAddress" tabindex="-1" role="dialog" aria-labelledby="modalAddressLabel"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <form method="POST" action="/client/profile/update2" class="modal-content">
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title" id="modalAddressLabel">{{Auth::user()->name}}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        Necesitamos que nos suministre estos datos
                        <label class="d-block mt-2 font-weight-bold">Dirección</label>
                        <input type="text" name="address" class="form-control @error('address') is-invalid @enderror">
                        @error('address')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <label class="d-block mt-2 font-weight-bold">Teléfono</label>
                        <input type="text" name="phone" class="form-control @error('phone') is-invalid @enderror">
                        @error('phone')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-primary">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    @endif
@endif
<script type="text/javascript" src="{{asset('js/jquery.min.js')}}"></script>
<script src="/plugins/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript">
    var plink="{{asset('/')}}";
    $.widget.bridge('uibutton', $.ui.button);
    $(function (){
        $('#modalAddress').modal('show');
    });
</script>
<script type="text/javascript" src="{{asset('js/popper.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/mdb.min.js')}}"></script>
<script src="/plugins/chart.js/Chart.min.js"></script>
<script src="/plugins/sparklines/sparkline.js"></script>
<script src="/plugins/jqvmap/jquery.vmap.min.js"></script>
<script src="/plugins/jqvmap/maps/jquery.vmap.usa.js"></script>
<script src="/plugins/jquery-knob/jquery.knob.min.js"></script>
<script src="/plugins/moment/moment.min.js"></script>
<script src="/plugins/daterangepicker/daterangepicker.js"></script>
<script src="/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<script src="/plugins/summernote/summernote-bs4.min.js"></script>
<script src="/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<script src="{{asset('js/adminlte.min.js')}}"></script>
<script src="{{ asset('js/app.js') }}"></script>
<script src="//cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
@if(session('success'))
    <script>
        swal({
            title:'Excelente',
            text:"{{session('success')}}",
            icon:'success',
            closeOnEsc: false,
            closeOnClickOutside: false
        })
    </script>
@endif
@if(Auth::user()->role!='admin')
<script type="text/javascript">
    var _smartsupp = _smartsupp || {};
    _smartsupp.key = '976b39271c9b86b680510c9d04c74f9ddcfcf385';
    window.smartsupp||(function(d) {
        var s,c,o=smartsupp=function(){ o._.push(arguments)};o._=[];
        s=d.getElementsByTagName('script')[0];c=d.createElement('script');
        c.type='text/javascript';c.charset='utf-8';c.async=true;
        c.src='https://www.smartsuppchat.com/loader.js?';s.parentNode.insertBefore(c,s);
    })(document);
    smartsupp('name', "{{Auth::user()->name}}");
    smartsupp('email', "{{Auth::user()->email}}");
    smartsupp('phone', '{{Auth::user()->phone}}');
</script>
@endif
@yield('footer_scripts')
</body>
</html>
