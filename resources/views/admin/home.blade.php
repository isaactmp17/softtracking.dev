@extends('layouts.app_dashboard')

@section('content')
    <div class="content-wrapper">
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Dashboard</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="/">Inicio</a></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <section class="content">
            <div class="container-fluid">
                <!-- Small boxes (Stat box) -->
                <div class="row">
                    <!-- ./col -->
                    <div class="col-lg-3 col-6">
                        <!-- small box -->
                        <div class="small-box bg-warning">
                            <div class="inner">
                                <h3>{{$shippings[0]}}</h3>
                                <p>Pendientes de recogida</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-clock"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-6">
                        <!-- small box -->
                        <div class="small-box bg-info">
                            <div class="inner">
                                <h3>{{$shippings[1]}}</h3>
                                <p>En camino</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-android-car"></i>
                            </div>
                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-3 col-6">
                        <!-- small box -->
                        <div class="small-box bg-success">
                            <div class="inner">
                                <h3>{{$shippings[2]}}</h3>

                                <p>Entregados</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-android-checkmark-circle"></i>
                            </div>
                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-3 col-6">
                        <!-- small box -->
                        <div class="small-box bg-danger">
                            <div class="inner">
                                <h3>{{$shippings[3]}}</h3>
                                <p>Anulados</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-android-close"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <!-- Left col -->
                    <section class="col-12 connectedSortable">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title mt-2">
                                    <i class="ion ion-clipboard mr-1"></i>
                                    Últimos envíos
                                </h3>

                                <div class="card-tools">
                                    <a href="/admin/shipping/create" class="btn btn-info float-right btn-sm"><i class="fas fa-plus"></i> Crear</a>
                                </div>
                            </div>
                            <div class="card-body table-responsive">
                                <table class="table table-bordered" id="DT_Shippings">
                                    <thead>
                                        <tr>
                                            <th>Track ID</th>
                                            <th>Estado</th>
                                            <th>Cliente</th>
                                            <th>Descripción</th>
                                            <th>Enviado por</th>
                                            <th>Direccion R</th>
                                            <th>Para</th>
                                            <th>Direccion E</th>
                                            <th>Teléfono</th>
                                            <th>Precio</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </section>
    </div>
@endsection
@section('footer_scripts')
    <script type="text/javascript">
        $(function () {
            var dtTable = $('#DT_Shippings').DataTable({
                language: {
                    url: '/lang/spanish.json'
                },
                processing: true,
                serverSide: true,
                responsive:true,
                ajax: "/admin",
                columns: [
                    {data: 'tracker_id', name: 'tracker_id'},
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                    {data: 'user', name: 'user'},
                    {data: 'description', name: 'description'},
                    {data: 'send_by', name: 'send_by'},
                    {data: 'address', name: 'address'},
                    {data: 'receipt_by', name: 'receipt_by'},
                    {data: 'address_receipt', name: 'address_receipt'},
                    {data: 'phone', name: 'phone'},
                    {data: 'price', name: 'price'}
                ]
            });
            $('body').on('change','.change-status',function(){
                $.ajax({
                    data:{'data':[$(this).data('target'),$(this).val()]},
                    url:'/admin/shipping/update_status',
                    type:'POST',
                    success:function (response){
                        swal({
                            title:'Excelente',
                            text:"El estado del envío ha sido actualizado correctamente.",
                            icon:'success',
                            closeOnEsc: false,
                            closeOnClickOutside: false
                        }).then((e) => {
                            if (e) {
                                window.location=window.location;
                            }
                        });

                    },
                    error:function (e){
                        alert('Hubo un problema al procesar su solicitud, intente de nuevamente más tarde.');
                    }
                })
            });

        });
    </script>
@endsection
