@extends('layouts.app_dashboard')

@section('content')
    <div class="content-wrapper">
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Crear envío</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="/">Inicio</a></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <section class="col-md-8 connectedSortable">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title mt-2">
                                    <i class="ion ion-clipboard mr-1"></i>
                                    Crear envío
                                </h3>
                                <div class="card-tools">
                                    <a href="/" class="btn btn-warning float-right btn-sm"><i class="fas fa-backward"></i> Volver</a>
                                </div>
                            </div>
                            <form action="{{(isset($item))?asset('admin/shipping/'.$item->id):asset('admin/shipping/')}}" method="POST">
                                @csrf
                                <div class="card-body">
                                    <label class="d-block font-weight-bold">Cliente</label>
                                    <select name="user_id" id="user_id" class="form-control" required>
                                        <option value="">Seleccione...</option>
                                        @foreach($clients as $client)
                                            <option value="{{$client->id}}" {{old('user_id', isset($item) ? $item->user_id : null)==$client->id?'selected':''}}>{{$client->name}}</option>
                                        @endforeach
                                    </select>
                                    <button type="button" class="btn btn-info btn-sm" id="copyData"><i class="fas fa-clipboard mr-2"></i> Copiar datos</button>

                                    <label class="d-block font-weight-bold mt-3">Dirección de recogida</label>
                                    <input type="text" name="address" class="form-control @error('address') is-invalid @enderror" value="{{ old('address', isset($item) ? $item->address : null) }}">
                                    @error('address')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror

                                    <label class="d-block font-weight-bold mt-3">Enviado por</label>
                                    <input type="text" name="send_by" class="form-control @error('send_by') is-invalid @enderror" value="{{ old('send_by', isset($item) ? $item->send_by : null) }}">
                                    @error('send_by')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror

                                    <label class="d-block font-weight-bold mt-3">Para</label>
                                    <input type="text" name="receipt_by" class="form-control @error('receipt_by') is-invalid @enderror" value="{{ old('receipt_by', isset($item) ? $item->receipt_by : null) }}">
                                    @error('receipt_by')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror

                                    <div class="form-group">
                                        <label>Descripción</label>
                                        <textarea class="form-control rounded-0 @error('description') is-invalid @enderror" name="description" rows="3">{{ old('description', isset($item) ? $item->description : null) }}</textarea>
                                    </div>
                                    @error('description')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror

                                    <label class="d-block font-weight-bold mt-3">Teléfono</label>
                                    <input type="tel" name="phone" class="form-control @error('phone') is-invalid @enderror" value="{{ old('phone', isset($item) ? $item->phone : null) }}">
                                    @error('phone')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror

                                    <label class="d-block font-weight-bold mt-3">Dirección de entrega</label>
                                    <input type="text" name="address_receipt" class="form-control @error('address_receipt') is-invalid @enderror" value="{{ old('address_receipt', isset($item) ? $item->address_receipt : null) }}">
                                    @error('address_receipt')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror

                                    <label class="d-block font-weight-bold mt-3">Precio</label>
                                    <input type="text" name="price" class="form-control validate-number @error('price') is-invalid @enderror" value="{{ old('price', isset($item) ? $item->price : 0) }}">
                                    <span id="prevPrice">0 COP</span>
                                    @error('price')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="card-footer clearfix">
                                    <button type="submit" class="btn btn-success">Crear</button>
                                    <button type="reset" class="btn btn-secondary">Limpiar</button>
                                </div>
                            </form>
                        </div>
                    </section>
                </div>
            </div>
        </section>
    </div>

@endsection
@section('footer_scripts')
    <script type="text/javascript">
        $(function () {
            $(".validate-number").keydown(function(event) {
                if(event.shiftKey)
                {
                    event.preventDefault();
                }

                if (event.keyCode == 46 || event.keyCode == 8)    {
                }
                else {
                    if (event.keyCode < 95) {
                        if (event.keyCode < 48 || event.keyCode > 57) {
                            event.preventDefault();
                        }
                    }
                    else {
                        if (event.keyCode < 96 || event.keyCode > 105) {
                            event.preventDefault();
                        }
                    }
                }
            });
        });
        $('#copyData').on('click',function (){
            var id=$('#user_id').val();
            if(id!=''){
                $.ajax({
                    url:'/admin/shipping',
                    data:{'id':id},
                    success:function (response){
                        $('input[name="send_by"]').val(response.name);
                        $('input[name="phone"]').val(response.phone);
                        $('input[name="address"]').val(response.address);
                        console.log(response);
                    }
                })
            }else{
                swal({
                    title:'',
                    text:'Seleccione un cliente.',
                    icon:'warning',
                })
            }
        })
        function formatMoney(number, decPlaces, decSep, thouSep) {
            decPlaces = isNaN(decPlaces = Math.abs(decPlaces)) ? 0 : decPlaces,
                decSep = typeof decSep === "undefined" ? "," : decSep;
            thouSep = typeof thouSep === "undefined" ? "." : thouSep;
            var sign = number < 0 ? "-" : "";
            var i = String(parseInt(number = Math.abs(Number(number) || 0).toFixed(decPlaces)));
            var j = (j = i.length) > 3 ? j % 3 : 0;

            return sign +
                (j ? i.substr(0, j) + thouSep : "") +
                i.substr(j).replace(/(\decSep{3})(?=\decSep)/g, "$1" + thouSep) +
                (decPlaces ? decSep + Math.abs(number - i).toFixed(decPlaces).slice(2) : "");
        }
        $('input[name="price"]').on('keyup',function (){
            $('#prevPrice').html(new Intl.NumberFormat("de-DE").format($(this).val())+' COP');
        })
        $('#prevPrice').html(new Intl.NumberFormat("de-DE").format($('input[name="price"]').val())+' COP');
    </script>
@endsection
