@extends('layouts.app')

@section('content')

    <div class="login-box">
        <div class="login-logo">
            <a href="#"><b>ATB</b> Panel</a>
        </div>
        <!-- /.login-logo -->
        <div class="card">
            <div class="card-body login-card-body">
                <p class="login-box-msg">Inicia sesión</p>

                <form  method="POST" action="{{ route('login') }}">
                    @csrf
                    <div class="input-group mb-3">
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="Correo" autofocus>

                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @else
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-envelope"></span>
                            </div>
                        </div>
                        @endif
                    </div>
                    <div class="input-group mb-3">
                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="Contraseña" required autocomplete="current-password">
                        @error('password')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-lock"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-8">
                            <div class="icheck-primary">
                                <input type="checkbox" id="remember">
                                <label for="remember">
                                    Recuérdame
                                </label>
                            </div>
                        </div>
                        <!-- /.col -->
                        <div class="col-4">
                            <button type="submit" class="btn btn-primary btn-block">Iniciar</button>
                        </div>
                    </div>
                </form>
                <div class="social-auth-links text-center mb-3">
                    <p>- O -</p>
                    <a href="{{asset('/login/facebook')}}" class="btn btn-block btn-primary">
                        <i class="fab fa-facebook mr-2"></i> ENTRA USANDO FACEBOOK
                    </a>
                    <a href="{{asset('/login/google')}}" class="btn btn-block btn-danger">
                        <i class="fab fa-google-plus mr-2"></i> ENTRA USANDO GOOGLE+
                    </a>
                </div>
                <!-- /.social-auth-links -->

                <p class="mb-0">
                    <a href="register" class="text-center">Registrarme con mi correo</a>
                </p>
            </div>
            <!-- /.login-card-body -->
        </div>
    </div>

@endsection
