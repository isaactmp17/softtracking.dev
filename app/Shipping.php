<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shipping extends Model
{
    protected $fillable = [
        'tracker_id', 'description', 'send_by','receipt_by','phone','address','address_receipt','price','user_id','status',
    ];
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
