<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Shipping;
use App\User;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('client');
    }

    public function index(Request $request)
    {
        if($request->ajax()){
            $items = Shipping::where('user_id',Auth::user()->id)->get();
            return Datatables::of($items)
                ->addColumn('action', function($item){
                    $status=['Anulado','Pendiente','En camino','Entregado'];
                    $color=['danger','warning','info','success'];
                    $html1='<small class="alert alert-'.$color[$item->status].' p-1 font-weight-bold">'.$status[$item->status].'</small>';
                    return $html1;
                })
                ->addColumn('price', function($item){
                    return '$'.number_format($item->price, 0,'','.');
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        $shippings=[Shipping::where('status',1)->where('user_id',Auth::user()->id)->count(),Shipping::where('status',2)->where('user_id',Auth::user()->id)->count(),Shipping::where('status',3)->where('user_id',Auth::user()->id)->count(),Shipping::where('status',0)->where('user_id',Auth::user()->id)->count()];
        return view('client.home',compact('shippings'));
    }

    public function update2(Request $request)
    {
        $request->validate([
            'address' => 'required|string|max:190',
            'phone' => 'required|max:20',
        ]);
        $item=User::findOrFail(Auth::user()->id);
        $item->address=$request->address;
        $item->phone=$request->phone;
        $item->save();

        return redirect()->to(Auth::user()->role)->with('success','Hemos guardado su dirección y teléfono correctamente.');
    }
}
