<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Shipping;
use Illuminate\Http\Request;
use DataTables;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    public function index(Request $request)
    {
        if($request->ajax()){
            $items = Shipping::with('user')->get();
            return Datatables::of($items)
                ->addColumn('user', function($item){
                    return $item->user->name;
                })
                ->addColumn('action', function($item){
                    $status=['Anulado','Pendiente','En camino','Entregado'];
                    $html1='<select class="form-control-sm change-status" data-target="'.$item->id.'">';
                    for($i=0;$i<4;$i++){
                        $selected=($i==$item->status)?'selected':'';
                        $html1.='<option value="'.$i.'" '.$selected.'>'.$status[$i].'</option>';
                    }
                    $html1.='</select>';
                    return $html1;
                })
                ->addColumn('price', function($item){
                    return '$'.number_format($item->price, 0,'','.');
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        $shippings=[Shipping::where('status',1)->count(),Shipping::where('status',2)->count(),Shipping::where('status',3)->count(),Shipping::where('status',0)->count()];
        return view('admin.home',compact('shippings'));
    }
}
