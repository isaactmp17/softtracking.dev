<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Shipping;
use App\User;
use Illuminate\Http\Request;

class ShippingController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    public function index(Request $request)
    {
        if($request->ajax()){
            $item=User::select('id','name','address','phone')->findOrFail($request->id);
            return $item;
        }
        abort(404);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $clients=User::where('role','client')->orderBy('name')->get();
        return view('admin.shipping_create_edit',compact('clients'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'user_id' => 'required',
            'send_by' => 'required|max:190',
            'receipt_by' => 'required|max:190',
            'description' => 'required|max:190',
            'phone' => 'required|max:20',
            'address' => 'required|max:190',
            'price' => 'required|max:190',
        ]);
        $item = new Shipping($request->all());
        $item->tracker_id=$this->trackGenerator();
        $item->save();

        return redirect()->to('/admin')->with('success','Envío creado correctamente.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function trackGenerator($length = 10){
        $band=0;
        do{
            $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $charactersLength = strlen($characters);
            $randomString = '';
            for ($i = 0; $i < $length; $i++) {
                $randomString .= $characters[rand(0, $charactersLength - 1)];
            }
            if(!Shipping::where('tracker_id',$randomString)->exists())
                $band=1;
        }while($band==0);

        return $randomString;
    }

    public function updateStatus(Request $request)
    {
        if($request->ajax()){
            $item=Shipping::findOrFail($request->data[0]);
            $item->status=$request->data[1];
            $item->save();

            return redirect()->to('/admin')->with('success','Estado del envío actualizado.');
        }
        abort(404);
    }
}
