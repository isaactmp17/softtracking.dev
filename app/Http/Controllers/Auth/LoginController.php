<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Laravel\Socialite\Facades\Socialite;
use App\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */

    protected function redirectTo()
    {
        return Auth::user()->role;
    }
    //protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function google()
    {
        return Socialite::driver('google')->redirect();
    }
    public function googleRedirect()
    {
        $user = Socialite::driver('google')->stateless()->user();
        if(User::where('gid',$user->id)->exists()) {
            $item = User::where('gid',$user->id)->first();
        }else{
            if(User::where('email',$user->email)->exists()){
                $item = User::where('email',$user->email)->first();
                $item->gid=$user->id;
                $item->save();
            }else{
                $item = new User();
                $item->email=$user->email;
                $item->name=$user->name;
                $item->gid=$user->id;
                $item->password=Hash::make($user->id);
                $item->save();
            }
        }
        Auth::login($item,true);

        return redirect()->to(Auth::user()->role);
    }

    public function facebook()
    {
        return Socialite::driver('facebook')->redirect();
    }
    public function facebookRedirect()
    {
        $user = Socialite::driver('facebook')->stateless()->user();
        if(User::where('fbid',$user->id)->exists()) {
            $item = User::where('fbid',$user->id)->first();
        }else{
            if(User::where('email',$user->email)->exists()){
                $item = User::where('email',$user->email)->first();
                $item->fbid=$user->id;
                $item->save();
            }else{
                $item = new User();
                $item->email=$user->email;
                $item->name=$user->name;
                $item->fbid=$user->id;
                $item->password=Hash::make($user->id);
                $item->save();
            }
        }
        Auth::login($item,true);

        return redirect()->to(Auth::user()->role);
    }
}
